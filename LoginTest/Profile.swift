//
//  Profile.swift
//  LoginTest
//
//  Created by BadBoy17 on 1/11/16.
//  Copyright © 2016 Formalytics. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Profile : UIViewController{
    
    var login: ViewController?
    
    var username: String?{
        didSet{
            navigationItem.title = username
        }
    }
    
    var imageURL: String?{
        didSet{
            print("Profile image was set from: \(imageURL!)")
            let image = URL(string: imageURL!)
            profileImageView.downloadedFrom(url: image!)
            view.addSubview(profileImageView)
            setupProfileImageView()
        }
    }
    
    
    
    override func viewDidLoad() {
        
        view.backgroundColor = .white
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "recent"), style: .plain, target: self, action: nil)
        
    }
    
    
    func handleLogout(){
        print("logging out\n")
        GIDSignIn.sharedInstance().signOut()
        dismiss(animated: true, completion: nil)
        
    }
    
    
    // PROFILE IMAGE VIEW
    lazy var profileImageView: UIImageView = {
        let piv = UIImageView()
        piv.translatesAutoresizingMaskIntoConstraints = false
        // fixes the scaling to be right with the image
        piv.contentMode = .scaleAspectFill
        // make it a circular image as we give it a rounded corners
        piv.layer.cornerRadius = ( self.view.frame.width - 40 )/2
        // makes the radius rounding come into effect
        piv.clipsToBounds = true
        return piv
    }()
    
    // set the constraints for the profile image view via ios9 constraints
    func setupProfileImageView(){
        // x:
        profileImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        // y:
        profileImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        // width:
        profileImageView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -40).isActive = true
        // height:
        profileImageView.heightAnchor.constraint(equalTo: view.widthAnchor, constant: -40).isActive = true
    }
}
