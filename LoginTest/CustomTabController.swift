//
//  CustomTabController.swift
//  LoginTest
//
//  Created by BadBoy17 on 1/11/16.
//  Copyright © 2016 Formalytics. All rights reserved.
//

import Foundation
import UIKit

class CustomTabController : UITabBarController{
    
    var profile : Profile? {
        didSet{
            let recentView = UINavigationController(rootViewController: profile!)
            recentView.tabBarItem.title = "Profile"
            recentView.tabBarItem.image = UIImage(named: "profile")
            
            let vc1 = UIViewController()
            let nc1 = UINavigationController(rootViewController: vc1)
            nc1.tabBarItem.title = "Dummy1"
            nc1.tabBarItem.image = UIImage(named: "people")
            
            let vc2 = UIViewController()
            let nc2 = UINavigationController(rootViewController: vc2)
            nc2.tabBarItem.title = "Dummy2"
            nc2.tabBarItem.image = UIImage(named: "settings")
            
            viewControllers = [recentView, nc1, nc2]
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.unselectedItemTintColor = UIColor(red: 90/255, green: 130/255, blue: 255/255, alpha: 1)
        tabBar.tintColor = .black
        tabBar.barTintColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
    }
    
    
    
}
