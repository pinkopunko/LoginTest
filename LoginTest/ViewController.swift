//
//  ViewController.swift
//  LoginTest
//
//  Created by BadBoy17 on 1/11/16.
//  Copyright © 2016 Formalytics. All rights reserved.
//

import UIKit
import Firebase


class ViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().signOut()
        
        //setup3Tabs()
        
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        // Uncomment to automatically sign in the user.
        //GIDSignIn.sharedInstance().signInSilently()
        
        // TODO(developer) Configure the sign-in button look/feel
        // ...
        
        view.addSubview(googleLoginButton)
        setupGoogleSignInButton()
        
    }
    
    let googleLoginButton : GIDSignInButton = {
        let b = GIDSignInButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
        
    }()
    
    func setupGoogleSignInButton(){
        // x: center
        googleLoginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        // y: center
        googleLoginButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        // width:
        googleLoginButton.widthAnchor.constraint(equalToConstant: view.frame.width / 3).isActive = true
        // height:
        googleLoginButton.heightAnchor.constraint(equalToConstant: googleLoginButton.frame.width / 3).isActive = true
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            //self.showMessagePrompt(error.localizedDescription)
            return
        }
        print("User Signed Into Google\n")
        
        let authentication = user.authentication
        let credential = FIRGoogleAuthProvider.credential(withIDToken: (authentication?.idToken)!,
                                                          accessToken: (authentication?.accessToken)!)
        
        let profile = Profile()
        profile.imageURL = user.profile.imageURL(withDimension: 400).absoluteString
        profile.login = self
        profile.username = user.profile.name
        
        let customTab = CustomTabController()
        customTab.profile = profile
        
        present(customTab, animated: true, completion: nil)
        
        FIRAuth.auth()?.signIn(with: credential) { (user, error) in
            print("User Signed Into DB\n")
            
        }
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        try! FIRAuth.auth()!.signOut()
        print("User Signed out of DB and Google\n")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

